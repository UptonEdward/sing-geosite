module sing-geosite

go 1.18

require (
	github.com/google/go-github/v45 v45.2.0
	github.com/sagernet/sing v0.2.10-0.20230802105922-c6a69b4912ee
	github.com/sagernet/sing-box v1.3.6
	github.com/sirupsen/logrus v1.9.3
	github.com/v2fly/v2ray-core/v5 v5.1.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/miekg/dns v1.1.55 // indirect
	github.com/sagernet/sing-dns v0.1.9-0.20230731012726-ad50da89b659 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/mod v0.11.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/tools v0.10.0 // indirect
)
